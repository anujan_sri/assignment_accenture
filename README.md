# Accenture

1	  When you enter the application can see the list of cities scroll down and click your city<br>
2   It will get you to secound activity it will show 5 days Forecast of that city<br>
3	  On offline also user can see the data.<br>
4   used technologies #Android, #Kotlin, #RetroFit,#Android-Room
5   I followed #material-design principles in android
<table>
  <tr>
    <td width="25%">
      <h4>Listing Cities mobile on Light mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210110_234902_com.anujan.accenture_n.jpg"   alt="Listing Cities mobile on Light mode" >
    </td>
    <td width="25%">
      <h4>Listing Weather mobile on Light mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210110_234905_com.anujan.accenture_n.jpg" style="width:300px" alt="Listing Weather mobile on Light mode">
    </td>
    <td width="25%">
      <h4>Listing Cities mobile on Dark mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210110_234914_com.anujan.accenture_n.jpg" alt="Listing Cities mobile on Dark mode">
    </td>
    <td width="25%">
      <h4>Listing Weather mobile on Dark mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210110_234917_com.anujan.accenture_n.jpg" alt="Listing Weather mobile on Dark mode">
    </td>
  </tr>
  <tr>
    <td width="25%">
      <h4>No data at Offline Light mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210111_000552_com.anujan.accenture_n.jpg" alt="No data at Offline Light mode">
    </td>
    <td width="25%">
      <h4>No Updated data at Offline Light mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210111_000547_com.anujan.accenture_n.jpg" alt="No Updated data at Offline Light mode">
    </td>
    <td width="25%">
      <h4>No data at Offline Dark mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210111_000532_com.anujan.accenture_n.jpg" alt="alt text">
    </td>
    <td width="25%">
      <h4>No Updated data at Offline Dark mode<h4></br>
      <img src="https://bitbucket.org/anujan_sri/assignment_accenture/raw/5edf35e7af790c8cfd04bd8828a6676be6382661/screenShots/Screenshot_20210111_000522_com.anujan.accenture_n.jpg" alt="alt text">
    </td>
  </tr>
</table>

